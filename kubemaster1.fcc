passwd:
  users:
    - name: core
      password_hash: "$1$BnXCT9Eg$oSz.VN5LpWLdRx0h468Mp0"
      ssh_authorized_keys:
        - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCrPRzNlmi+RDz25kt+ujV0wBRXaczOQA9ACt6L8QQT/7057UK8j+6nwZPI18Epe6ZC4Y4HcEgaQEUmiTCU/wkLB+kY79XaDq0NrSqbsPDh+KbUONIuVLEQePyKIXd040f26uCuHUm7iaPAG2fvE6F2URdfyxojKpl+6cdeMD4tGJ+CYuSKAmbHb8UQBmc404kbfn5/pg0IS9rIDYhqHPKmfmAeEsZhWE/HepEtHjUUj2G4Ja9FC9MXwApROpgcDZhCLO74IMaDq/XttwEo53huMSKLqKOGfakcAImp9XTC2jdANaeHl05EUCzVMw6B1DoPwd9wI12ThGa1lDftRYQOyLLGCVP+eDJbE63eIk6SSntIg4s2sc/4aDlZMzSUnMQjYL1OxcjXchiWn/onmsYl97QdmwbvIbdtNpg+/3aPx5tQfG5HzvWDsC9DfmlmBStWKY85LRGRL2Hte8uF/1ccHR4P9hqpihiclEAycKb0o/E8T0JtVdxAES8KrbmUghM= joao@itsafuckinpc
      groups: [ sudo, docker ]
storage:
  directories:
  - path: /opt
    filesystem: root
    mode: 0755
    user: 
      name: core
  - path: /etc/kubernetes
    filesystem: root
    mode: 0755
    user: 
      name: core
  - path: /etc/kubernetes/certificados
    filesystem: root
    mode: 0755
    user:
      name: core
  - path: /etc/kubernetes/kubeconfig
    filesystem: root
    mode: 0755
    user:
      name: core
  files:
  - path: /etc/hostname
    filesystem: root
    mode: 0420
    overwrite: true
    contents:
      inline: |
        kubemaster1
  - path: /etc/hosts
    filesystem: root
    mode: 0644
    overwrite: true
    contents:
      inline: |
        127.0.0.1 localhost
        192.168.122.200 etcd1
        192.168.122.201 etcd2
        192.168.122.202 etcd3
        192.168.122.203 kubemaster1
        192.168.122.204 kubemaster2
        192.168.122.205 worker1
        192.168.122.206 worker2
        192.168.122.207 worker3
  - path: /etc/flatcar/update.conf
    filesystem: root
    mode: 0644
    overwrite: true
    contents:
      inline: |
        REBOOT_STRATEGY=off
networkd:
  units:
    - name: 00-eth0.network
      contents: |
        [Match]
        Name=eth0

        [Network]
        Address=192.168.122.203/24
        Gateway=192.168.122.1
        DNS=8.8.8.8        
systemd:
  units:
    - name: kube-apiserver.service
      enabled: true
      contents: |
        [Unit]
        Description=Kubernetes API Server
        
        [Service]
        User=core
        ExecStartPre=-/bin/docker kill apiserver
        ExecStartPre=-/bin/docker rm apiserver
        ExecStartPre=-/bin/docker pull gcr.io/google-containers/kube-apiserver-amd64:v1.18.3
        ExecStart=/bin/docker run --name apiserver \
                --volume /etc/localtime:/etc/localtime:ro \
                --volume /etc/kubernetes:/etc/kubernetes:ro \
                --net host \
                gcr.io/google-containers/kube-apiserver-amd64:v1.18.3 \
                /usr/local/bin/kube-apiserver \
                --bind-address=0.0.0.0 \
                --etcd-servers=https://etcd1:2379 \
                --etcd-cafile=/etc/kubernetes/certificados/ca.crt \
                --etcd-certfile=/etc/kubernetes/certificados/etcd.crt \
                --etcd-keyfile=/etc/kubernetes/certificados/etcd.key \
                --service-account-key-file=/etc/kubernetes/certificados/serviceaccount.pub \
                --tls-cert-file=/etc/kubernetes/certificados/kubernetes.crt \
                --tls-private-key-file=/etc/kubernetes/certificados/kubernetes.key \
                --kubelet-client-certificate=/etc/kubernetes/certificados/kubernetes.crt \
                --kubelet-client-key=/etc/kubernetes/certificados/kubernetes.key \
                --kubelet-https=true \
                --client-ca-file=/etc/kubernetes/certificados/ca.crt \
                --proxy-client-cert-file=/etc/kubernetes/certificados/serviceaccount.crt \
                --proxy-client-key-file=/etc/kubernetes/certificados/serviceaccount.key \
                --service-cluster-ip-range=10.254.0.0/16 \
                --enable-admission-plugins=NamespaceLifecycle,LimitRanger,ServiceAccount,ResourceQuota,PodSecurityPolicy \
                --authorization-mode=RBAC \
                --advertise-address=192.168.122.203 \
                --allow-privileged=true \
                -v=0
        Restart=on-failure
        ExecStop=/bin/docker stop apiserver

        [Install]
        WantedBy=multi-user.target
    - name: kube-controller-manager.service
      enabled: true
      contents: |
        [Unit]
        Description=Kubernetes Controller Manager
        After=kube-apiserver.service

        [Service]
        User=core
        ExecStartPre=-/bin/docker kill controller-manager
        ExecStartPre=-/bin/docker rm controller-manager
        ExecStartPre=-/bin/docker pull gcr.io/google-containers/kube-controller-manager-amd64:v1.18.3
        ExecStart=/bin/docker run --name controller-manager \
                --volume /etc/localtime:/etc/localtime:ro \
                --volume /etc/kubernetes:/etc/kubernetes:ro \
                --net=host \
                gcr.io/google-containers/kube-controller-manager-amd64:v1.18.3 \
                /usr/local/bin/kube-controller-manager \
                --kubeconfig=/etc/kubernetes/kubeconfig/controller-manager.kubeconfig \
                --cluster-name=default \
                --cluster-signing-cert-file=/etc/kubernetes/certificados/ca.crt \
                --cluster-signing-key-file=/etc/kubernetes/certificados/ca.key \
                --root-ca-file=/etc/kubernetes/certificados/ca.crt \
                --service-account-private-key-file=/etc/kubernetes/certificados/serviceaccount.key \
                --use-service-account-credentials=true \
                --cluster-cidr=18.254.0.0/16 \
                --flex-volume-plugin-dir="/var/lib/kubelet/kubelet-plugins/volume/"
        Restart=on-failure
        ExecStop=/bin/docker stop controller-manager

        [Install]
        WantedBy=multi-user.target
    - name: kube-scheduler.service
      enabled: true
      contents: |
        [Unit]
        Description=Kubernetes Scheduler Plugin
        After=kube-apiserver.service

        [Service]
        User=core
        ExecStartPre=-/bin/docker kill scheduler
        ExecStartPre=-/bin/docker rm scheduler
        ExecStartPre=-/bin/docker pull gcr.io/google-containers/kube-scheduler-amd64:v1.18.3
        ExecStart=/bin/docker run --name scheduler \
                --volume /etc/localtime:/etc/localtime:ro \
                --volume /etc/kubernetes:/etc/kubernetes:ro \
                --net=host \
                gcr.io/google-containers/kube-scheduler-amd64:v1.18.3 \
                /usr/local/bin/kube-scheduler \
                --config=/etc/kubernetes/kubeconfig/kube-scheduler.yaml
        Restart=on-failure
        ExecStop=/bin/docker stop scheduler

        [Install]
        WantedBy=multi-user.target
    - name: kube-proxy.service
      enabled: true
      contents: |
        [Unit]
        Description=Kubernetes Kube-Proxy Server
        After=kube-apiserver.service

        [Service]
        User=core
        ExecStartPre=-/bin/docker kill proxy
        ExecStartPre=-/bin/docker rm proxy
        ExecStartPre=-/bin/docker pull gcr.io/google-containers/kube-proxy-amd64:v1.18.3
        ExecStart=/bin/docker run --name proxy \
                --volume /etc/kubernetes:/etc/kubernetes \
                --net=host \
                --pid=host \
                --privileged \
                gcr.io/google-containers/kube-proxy-amd64:v1.18.3 \
                /usr/local/bin/kube-proxy \
                --config=/etc/kubernetes/kubeconfig/kube-proxy.yaml
        Restart=on-failure
        ExecStop=/bin/docker stop proxy

        [Install]
        WantedBy=multi-user.target
    - name: kubelet.service
      enabled: true
      contents: |
        [Unit]
        Description=Kubelet Service
        After=kube-apiserver.service
        
        [Service]
        User=root
        TimeoutSec=600
        ExecStartPre=-/bin/docker kill kubelet
        ExecStartPre=-/bin/docker rm kubelet
        ExecStartPre=-/bin/docker pull gcr.io/google-containers/hyperkube-amd64:v1.18.3
        ExecStart=/bin/docker run --name kubelet \
                --volume /etc/localtime:/etc/localtime:ro \
                --volume /:/rootfs:ro \
                --volume /sys:/sys:ro \
                --volume /etc/kubernetes:/etc/kubernetes:ro \
                --volume /var/lib:/var/lib:rw,rshared \
                --volume /var/run/docker.sock:/var/run/docker.sock:rw \
                --volume /etc/cni:/etc/cni:rw \
                --volume /opt/cni:/opt/cni:rw \
                --pid=host \
                --net=host \
                --privileged=true \
                gcr.io/google-containers/hyperkube-amd64:v1.18.3 kubelet \
                --cgroup-driver=cgroupfs \
                --port=10250 \
                --kubeconfig=/etc/kubernetes/kubeconfig/kubemaster1.kubeconfig \
                --cluster-dns=10.254.0.10 \
                --volume-plugin-dir="/var/lib/kubelet/kubelet-plugins/volume/exec" \
               --hostname-override=kubemaster1 \
               --cni-conf-dir=/etc/cni/net.d \
               --cni-bin-dir=/opt/cni/bin \
               --network-plugin=cni \
               --cluster-domain=cluster.local
        Restart=on-failure
        ExecStop=/bin/docker stop kubelet

        [Install]
        WantedBy=multi-user.target
    - name: timezone.service
      enabled: true
      contents: |
        [Unit]
        Description=Ajusta timezone
        After=network.target

        [Service]
        User=root
        Type=oneshot
        ExecStart=/usr/bin/timedatectl set-timezone America/Sao_Paulo

        [Install]
        WantedBy=multi-user.target

